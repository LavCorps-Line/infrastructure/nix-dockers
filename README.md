# LavCorps-Line NixOS Dockers

---

To use the latest stable image, use:

* `registry.gitlab.com/lavcorps-line/infrastructure/nox-dockers/23.05:stable`
    * This image contains NixOS, v23.05!

The latest stable image will be periodically regenerated for the latest updates.

---

To use the latest (unstable!) image, use:

* `registry.gitlab.com/lavcorps-line/infrastructure/nox-dockers/23.05:latest`
    * This image contains NixOS, v23.05!

Unstable images are generated once, upon commit.
